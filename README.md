# Plasmoids

Personal tweaks for some plasmoids.

- Task manager
  - [x] compact minimized tasks
  - [x] compact tasks from other desktops
  - [x] transparent tasks from other desktops
  - [ ] options to toggle it

![collapsed tasks](https://gitlab.com/uploads/-/system/personal_snippet/1885334/5b8774d1ebd2a14cd4a60c306e1b846d/Screenshot_20190814_071055.png)

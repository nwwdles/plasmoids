#!/usr/bin/env bash
set -euo

TARGETDIR="$HOME/.local/share/plasma/plasmoids"

copy() {
    local src=$(readlink -f "$1")
    local target="$TARGETDIR/$1"
    rm -rf "$target"
    # mkdir -p "$target"
    # cp -rT "$1" "$target"
    ln -sfn "$src" "$target"
}

copy "org.kde.plasma.colorpicker"
copy "org.kde.plasma.taskmanager"
copy "org.kde.plasma.private.systemtray"

kquitapp5 plasmashell && kstart5 plasmashell
